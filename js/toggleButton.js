function extractActionsFromDataset(dataset) {
  let targets = dataset.target.split(" ");
  let actions = dataset.action.split(" ");
  let targetClasses = dataset.tgclass.split(" ");
  return targets.map((t, i) => {
    return {
      target: t,
      action: actions[i],
      tgclass: targetClasses[i],
      scrollIntoView: !!("scrollview" in dataset) && i == 0,
      trackEvent: !!("trackingname" in dataset) && i == 0,
      trackingCategory: dataset.trackingcategory || "Button event",
      trackingName: dataset.trackingname
    };
  });
}

class ToggleButton extends HTMLElement {
  constructor() {
    super();

    let btnActions = extractActionsFromDataset(this.dataset);

    this.onclick = (e) => {
      btnActions.forEach((a) => {
        let targetEl = document.querySelector(a.target);
        targetEl?.classList[a.action](a.tgclass);
        if (a.scrollIntoView) targetEl?.scrollIntoView();
        if (!!a.trackEvent) {
          _paq.push([
            "trackEvent",
            a.trackingCategory,
            a.trackingName,
            window.location.pathname
          ]);
        }
      });
    };
  }
}

export default ToggleButton;
